##
##  rhlibrary
##
##  The Radio Helsinki Rivendell Import Daemon
##
##
##  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
##
##  This file is part of rhlibrary.
##
##  rhlibrary is free software: you can redistribute it and/or modify
##  it under the terms of the GNU General Public License as published by
##  the Free Software Foundation, either version 3 of the License, or
##  any later version.
##
##  rhlibrary is distributed in the hope that it will be useful,
##  but WITHOUT ANY WARRANTY; without even the implied warranty of
##  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
##  GNU General Public License for more details.
##
##  You should have received a copy of the GNU General Public License
##  along with rhlibrary. If not, see <http://www.gnu.org/licenses/>.
##

curdir:= $(shell pwd)
GOCMD := GOPATH=$(curdir) go
ifdef GOROOT
GOCMD = GOPATH=$(curdir) $(GOROOT)/bin/go
endif
BUILDOPTS := -tags $(shell pkg-config --modversion gtk+-3.0 | sed 's/^\(.*\)\.\(.*\)\..*/gtk_\1_\2/')

EXECUTEABLE := rhlibrary

LIBS := "code.helsinki.at/rhrd-go/rddb" \
        "code.helsinki.at/rhrd-go/player" \
        "github.com/gotk3/gotk3/glib" \
        "github.com/gotk3/gotk3/gtk"


.PHONY: getlibs updatelibs vet format build clean distclean
all: build


getlibs:
	@$(foreach lib,$(LIBS), echo "fetching lib: $(lib)"; $(GOCMD) get -d $(lib);)

updatelibs:
	@$(foreach lib,$(LIBS), echo "updating lib: $(lib)"; $(GOCMD) get -d -u $(lib);)

vet:
	@echo "vetting: $(EXECUTEABLE)"
	@$(GOCMD) vet $(EXECUTEABLE)

format:
	@echo "formating: $(EXECUTEABLE)"
	@$(GOCMD) fmt $(EXECUTEABLE)

build: getlibs
	@echo "installing: $(EXECUTEABLE)"
	@$(GOCMD) install $(BUILDOPTS) $(EXECUTEABLE)


clean:
	rm -rf pkg/*/$(EXECUTEABLE)
	rm -rf bin

distclean: clean
	@$(foreach dir,$(shell ls src/),$(if $(subst $(EXECUTEABLE),,$(dir)),$(shell rm -rf src/$(dir))))
	rm -rf pkg
