//
//  rhlibrary
//
//  The Radio Helsinki Rivendell Library
//
//
//  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of rhlibrary.
//
//  rhlibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  rhlibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with rhlibrary. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"fmt"
	"sort"
	"strconv"
	"sync"
	"time"

	"code.helsinki.at/rhrd-go/player"
	"code.helsinki.at/rhrd-go/rddb"
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
)

var (
	showsMutex       sync.Mutex
	shows            rddb.ShowList
	showsComboBox    *gtk.ComboBoxText
	showsCurrentID   uint
	showsCartListBin *gtk.Bin
	showsCartList    *gtk.Grid
)

func fetchShows(db *rddb.DB) (err error) {
	showsMutex.Lock()
	defer showsMutex.Unlock()

	if shows, err = db.GetShowList(rhuser.Username); err != nil {
		return err
	}
	sort.Sort(shows)
	return
}

func updateShows(db *rddb.DB) (err error) {
	if err = fetchShows(db); err != nil {
		return
	}

	showsComboBox.RemoveAll()
	for _, show := range shows {
		name := fmt.Sprintf("     %d   |   %s       (%s, %v, %s, %d Min.)", show.ID, show.TitleFull, show.Rhythm, show.Dow, show.StartTime, show.Length)
		showsComboBox.Append(strconv.FormatUint(uint64(show.ID), 10), name)
	}

	if !showsComboBox.SetActiveID(strconv.FormatUint(uint64(showsCurrentID), 10)) {
		showsComboBox.SetActive(0)
	}
	return
}

func selectShow() rddb.ShowListEntry {
	id := showsComboBox.GetActiveID()
	if id == "" {
		return rddb.ShowListEntry{}
	}
	showid64, err := strconv.ParseUint(id, 10, 32)
	if err != nil {
		rhdl.Printf("invalid show-id in combo box: %v", err)
		return rddb.ShowListEntry{}
	}
	showsCurrentID = uint(showid64)

	showsMutex.Lock()
	defer showsMutex.Unlock()

	for _, show := range shows {
		if show.ID == showsCurrentID {
			return show
		}
	}
	return rddb.ShowListEntry{}
}

func addShowComboBox(grid *gtk.Grid, db *rddb.DB, p *player.Player) (err error) {
	var box *gtk.Box
	if box, err = gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 10); err != nil {
		return
	}

	var label *gtk.Label
	if label, err = gtk.LabelNew("Sendung auswählen"); err != nil {
		return
	}
	box.PackStart(label, false, false, 0)

	if showsComboBox, err = gtk.ComboBoxTextNew(); err != nil {
		return
	}
	if err = updateShows(db); err != nil {
		return err
	}
	showsComboBox.Connect("changed", func(_ *gtk.ComboBoxText) {
		show := selectShow()
		if show.ID == 0 {
			return
		}

		glib.IdleAdd(func() {
			if err = updateShowCartListRows(show, db, p); err != nil {
				rhdl.Printf("error loading show carts for %d: %v", show.ID, err)
				return
			}
		})
		rhdl.Printf("show %d is now selected: %s", show.ID, show.Title)
	})

	box.PackStart(showsComboBox, true, true, 0)
	box.SetHExpand(true)

	grid.Attach(box, 1, 1, 1, 1)
	return
}

func getShowCartListLabel(caption, css string) (label *gtk.Label, err error) {
	if label, err = gtk.LabelNew(caption); err != nil {
		return
	}
	if err = setCssStyle(&label.Widget, css); err != nil {
		return
	}

	return
}

func getShowCartListEntryLabel(caption string, idx int) (label *gtk.Label, err error) {
	if (idx & 1) == 1 {
		return getShowCartListLabel(caption, ".label { background-color: #D2D2D2; padding: 7px 15px; }")
	} else {
		return getShowCartListLabel(caption, ".label { padding: 7px 15px; }")
	}
}

func getShowCartListHeadLabel(caption string) (label *gtk.Label, err error) {
	return getShowCartListLabel(caption, `
     .label {
        border-bottom: 2px solid;
        padding: 8px 15px;
        font-weight: bold;
     }`)
}

func getShowCartListEntryPlayButton(cart rddb.CartListEntry, p *player.Player, idx int) (gtk.IWidget, error) {
	btn, err := gtk.ButtonNewFromIconName("media-playback-start", gtk.ICON_SIZE_SMALL_TOOLBAR)
	if err != nil {
		return nil, err
	}
	btn.SetSizeRequest(35, 35)

	var frame *gtk.Frame
	if frame, err = gtk.FrameNew(""); err != nil {
		return nil, err
	}

	css := ".frame { border: 0; padding: 4px 15px; }"
	if (idx & 1) == 1 {
		css = ".frame { border: 0; background-color: #D2D2D2; padding: 4px 15px; }"
	}
	if err = setCssStyle(&frame.Widget, css); err != nil {
		return nil, err
	}
	frame.Add(btn)

	if len(cart.Cuts) < 1 {
		btn.SetSensitive(false)
		return frame, nil
	}

	btn.Connect("clicked", func(_ *gtk.Button) {
		if err := p.Load(cart.Number, cart.Cuts[0].Number); err != nil {
			rhdl.Println(err) // TODO: feedback at GUI?
			return
		}
		if err := p.Play(); err != nil {
			rhdl.Println(err) // TODO: feedback at GUI?
		}
	})

	if len(cart.Cuts) > 1 {
		rhl.Println("ingoring additional cuts for show cart", cart.Number)
	}

	return frame, nil
}

func addShowCartListEntry(idx int, cart rddb.CartListEntry, p *player.Player) (err error) {
	var lcart *gtk.Label
	if lcart, err = getShowCartListEntryLabel(strconv.FormatUint(uint64(cart.Number), 10), idx); err != nil {
		return
	}
	showsCartList.Attach(lcart, 1, idx, 1, 1)

	title := "-"
	if cart.Exists {
		title = cart.Title
	}
	var ltitle *gtk.Label
	if ltitle, err = getShowCartListEntryLabel(title, idx); err != nil {
		return
	}
	ltitle.SetHExpand(true)
	showsCartList.Attach(ltitle, 2, idx, 1, 1)

	duration := "-"
	imported := "-"
	numplayed := "-"
	lastplayed := "-"
	if len(cart.Cuts) >= 1 {
		duration = time.Unix(0, 0).UTC().Add(cart.Cuts[0].Duration).Format("15:04:05.0")
		if cart.Cuts[0].Imported.Valid {
			imported = cart.Cuts[0].Imported.Time.Format("Mon 02.01.2006 15:04:05")
		}

		numplayed = strconv.FormatUint(uint64(cart.Cuts[0].NumPlayed), 10)
		if cart.Cuts[0].LastPlayed.Valid {
			lastplayed = cart.Cuts[0].LastPlayed.Time.Format("Mon 02.01.2006 15:04:05")
		}
	}

	var lduration *gtk.Label
	if lduration, err = getShowCartListEntryLabel(duration, idx); err != nil {
		return
	}
	showsCartList.Attach(lduration, 3, idx, 1, 1)

	var limported *gtk.Label
	if limported, err = getShowCartListEntryLabel(imported, idx); err != nil {
		return
	}
	showsCartList.Attach(limported, 4, idx, 1, 1)

	var lnumplayed *gtk.Label
	if lnumplayed, err = getShowCartListEntryLabel(numplayed, idx); err != nil {
		return
	}
	showsCartList.Attach(lnumplayed, 5, idx, 1, 1)

	var llastplayed *gtk.Label
	if llastplayed, err = getShowCartListEntryLabel(lastplayed, idx); err != nil {
		return
	}
	showsCartList.Attach(llastplayed, 6, idx, 1, 1)

	var btn gtk.IWidget
	if btn, err = getShowCartListEntryPlayButton(cart, p, idx); err != nil {
		return
	}
	showsCartList.Attach(btn, 7, idx, 1, 1)
	return
}

func addShowCartListHead(idx int) (err error) {
	var lcart *gtk.Label
	if lcart, err = getShowCartListHeadLabel("Cart #"); err != nil {
		return
	}
	showsCartList.Attach(lcart, 1, idx, 1, 1)

	var ltitle *gtk.Label
	if ltitle, err = getShowCartListHeadLabel("Titel"); err != nil {
		return
	}
	ltitle.SetHExpand(true)
	showsCartList.Attach(ltitle, 2, idx, 1, 1)

	var lduration *gtk.Label
	if lduration, err = getShowCartListHeadLabel("Länge"); err != nil {
		return
	}
	showsCartList.Attach(lduration, 3, idx, 1, 1)

	var limported *gtk.Label
	if limported, err = getShowCartListHeadLabel("importiert"); err != nil {
		return
	}
	showsCartList.Attach(limported, 4, idx, 1, 1)

	var lnumplayed *gtk.Label
	if lnumplayed, err = getShowCartListHeadLabel("# gespielt"); err != nil {
		return
	}
	showsCartList.Attach(lnumplayed, 5, idx, 1, 1)

	var llastplayed *gtk.Label
	if llastplayed, err = getShowCartListHeadLabel("zuletzt gespielt"); err != nil {
		return
	}
	showsCartList.Attach(llastplayed, 6, idx, 1, 1)

	var lplay *gtk.Label
	if lplay, err = getShowCartListHeadLabel("Play"); err != nil {
		return
	}
	showsCartList.Attach(lplay, 7, idx, 1, 1)
	return
}

func updateShowCartListRows(show rddb.ShowListEntry, db *rddb.DB, p *player.Player) (err error) {
	if showsCartList != nil {
		showsCartListBin.Remove(showsCartList)
	}

	if showsCartList, err = gtk.GridNew(); err != nil {
		return
	}
	showsCartList.SetHExpand(true)
	showsCartList.SetVExpand(true)

	carts, err := db.GetShowCartList(show)
	if err != nil {
		return err
	}

	addShowCartListHead(1)
	for idx, cart := range carts {
		addShowCartListEntry(idx+2, cart, p)
	}

	showsCartListBin.Add(showsCartList)
	showsCartList.ShowAll()
	return
}

func addShowCartList(grid *gtk.Grid, db *rddb.DB, p *player.Player) (err error) {
	var frame *gtk.Frame
	if frame, err = gtk.FrameNew(""); err != nil {
		return
	}
	showsCartListBin = &frame.Bin
	if err = setCssStyle(&frame.Widget, ".frame { border: 0; }"); err != nil {
		return
	}

	var sw *gtk.ScrolledWindow
	if sw, err = gtk.ScrolledWindowNew(nil, nil); err != nil {
		return
	}
	sw.SetHExpand(true)
	sw.SetVExpand(true)
	sw.Add(frame)

	grid.Attach(sw, 1, 2, 1, 1)
	return
}

func getShowsSelectorWidget(db *rddb.DB, p *player.Player) (gtk.IWidget, error) {
	grid, err := gtk.GridNew()
	if err != nil {
		return nil, err
	}
	grid.SetRowSpacing(25)
	grid.SetHExpand(true)
	grid.SetVExpand(true)

	if err = addShowComboBox(grid, db, p); err != nil {
		return nil, err
	}
	if err = addShowCartList(grid, db, p); err != nil {
		return nil, err
	}

	var frame *gtk.Frame
	if frame, err = gtk.FrameNew(""); err != nil {
		return nil, err
	}
	if err = setCssStyle(&frame.Widget, ".frame { border: 0; padding: 25px 42px; }"); err != nil {
		return nil, err
	}

	frame.Add(grid)

	return frame, nil
}
