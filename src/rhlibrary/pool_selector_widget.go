//
//  rhlibrary
//
//  The Radio Helsinki Rivendell Library
//
//
//  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of rhlibrary.
//
//  rhlibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  rhlibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with rhlibrary. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"fmt"
	"sort"
	"strconv"
	"sync"
	"time"

	"code.helsinki.at/rhrd-go/player"
	"code.helsinki.at/rhrd-go/rddb"
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
)

const (
	POOLS_COL_CART = iota
	POOLS_COL_ARTIST
	POOLS_COL_TITLE
	POOLS_COL_ALBUM
	POOLS_COL_DURATION
	POOLS_COL_IMPORTED
	POOLS_COL_NUM_PLAYED
	POOLS_COL_LAST_PLAYED
)

var (
	poolsMutex         sync.Mutex
	pools              rddb.PoolList
	poolsComboBox      *gtk.ComboBoxText
	poolsCurrentGroup  string
	PoolsCartListStore *gtk.ListStore
)

func fetchPools(db *rddb.DB) (err error) {
	poolsMutex.Lock()
	defer poolsMutex.Unlock()

	if pools, err = db.GetPoolList(rhuser.Username); err != nil {
		return err
	}
	sort.Sort(pools)
	return
}

func updatePools(db *rddb.DB) (err error) {
	if err = fetchPools(db); err != nil {
		return
	}

	poolsComboBox.RemoveAll()
	for _, pool := range pools {
		name := fmt.Sprintf("     %s   |   %s", pool.ShortName, pool.Title)
		poolsComboBox.Append(pool.Group, name)
	}

	if !poolsComboBox.SetActiveID(poolsCurrentGroup) {
		poolsComboBox.SetActive(0)
	}
	return
}

func selectPool() rddb.PoolListEntry {
	id := poolsComboBox.GetActiveID()
	if id == "" {
		return rddb.PoolListEntry{}
	}
	poolsCurrentGroup = id

	poolsMutex.Lock()
	defer poolsMutex.Unlock()

	for _, pool := range pools {
		if pool.Group == poolsCurrentGroup {
			return pool
		}
	}
	return rddb.PoolListEntry{}
}

func addPoolComboBox(grid *gtk.Grid, db *rddb.DB, p *player.Player) (err error) {
	var box *gtk.Box
	if box, err = gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 10); err != nil {
		return
	}

	var label *gtk.Label
	if label, err = gtk.LabelNew("Musik-Pool auswählen"); err != nil {
		return
	}
	box.PackStart(label, false, false, 0)

	if poolsComboBox, err = gtk.ComboBoxTextNew(); err != nil {
		return
	}
	if err = updatePools(db); err != nil {
		return err
	}
	poolsComboBox.Connect("changed", func(_ *gtk.ComboBoxText) {
		pool := selectPool()
		if pool.Group == "" {
			return
		}

		glib.IdleAdd(func() {
			if err = updatePoolCartListRows(pool, db, p); err != nil {
				rhdl.Printf("error loading pool carts for %s: %v", pool.Group, err)
				return
			}
		})
		rhdl.Printf("pool %s is now selected: %s", pool.Group, pool.Title)
	})

	box.PackStart(poolsComboBox, true, true, 0)
	box.SetHExpand(true)

	grid.Attach(box, 1, 1, 1, 1)
	return
}

func updatePoolCartListRows(pool rddb.PoolListEntry, db *rddb.DB, p *player.Player) (err error) {
	carts, err := db.GetPoolCartList(pool)
	if err != nil {
		return err
	}

	PoolsCartListStore.Clear()
	for num, cart := range carts {
		iter := PoolsCartListStore.Append()

		duration := time.Unix(0, 0).UTC().Add(cart.Cuts[0].Duration).Format("15:04:05.0")
		imported := "-"
		if cart.Cuts[0].Imported.Valid {
			imported = cart.Cuts[0].Imported.Time.Format("Mon 02.01.2006 15:04:05")
		}

		numplayed := strconv.FormatUint(uint64(cart.Cuts[0].NumPlayed), 10)
		lastplayed := "-"
		if cart.Cuts[0].LastPlayed.Valid {
			lastplayed = cart.Cuts[0].LastPlayed.Time.Format("Mon 02.01.2006 15:04:05")
		}

		err = PoolsCartListStore.Set(iter,
			[]int{POOLS_COL_CART, POOLS_COL_ARTIST, POOLS_COL_TITLE, POOLS_COL_ALBUM, POOLS_COL_DURATION, POOLS_COL_IMPORTED, POOLS_COL_NUM_PLAYED, POOLS_COL_LAST_PLAYED},
			[]interface{}{fmt.Sprintf("%d", num), cart.Artist, cart.Title, cart.Album, duration, imported, numplayed, lastplayed})
		if err != nil {
			return
		}
	}

	return
}

func appendPoolCartListColumn(tree *gtk.TreeView, title string, id int) (err error) {
	var cell *gtk.CellRendererText
	if cell, err = gtk.CellRendererTextNew(); err != nil {
		return
	}

	var col *gtk.TreeViewColumn
	if col, err = gtk.TreeViewColumnNewWithAttribute(title, cell, "text", id); err != nil {
		return
	}
	col.SetSortColumnID(id)
	col.SetExpand(true)
	tree.AppendColumn(col)
	return
}

func addPoolCartList(grid *gtk.Grid, db *rddb.DB, p *player.Player) (err error) {
	var tree *gtk.TreeView
	if tree, err = gtk.TreeViewNew(); err != nil {
		return
	}

	appendPoolCartListColumn(tree, "Cart #", POOLS_COL_CART)
	appendPoolCartListColumn(tree, "Interpret", POOLS_COL_ARTIST)
	appendPoolCartListColumn(tree, "Titel", POOLS_COL_TITLE)
	appendPoolCartListColumn(tree, "Album", POOLS_COL_ALBUM)
	appendPoolCartListColumn(tree, "Länge", POOLS_COL_DURATION)
	appendPoolCartListColumn(tree, "importiert", POOLS_COL_IMPORTED)
	appendPoolCartListColumn(tree, "# gespielt", POOLS_COL_NUM_PLAYED)
	appendPoolCartListColumn(tree, "zuletzt gespielt", POOLS_COL_LAST_PLAYED)

	if PoolsCartListStore, err = gtk.ListStoreNew(glib.TYPE_STRING, glib.TYPE_STRING, glib.TYPE_STRING, glib.TYPE_STRING,
		glib.TYPE_STRING, glib.TYPE_STRING, glib.TYPE_STRING, glib.TYPE_STRING); err != nil {
		return
	}
	tree.SetModel(PoolsCartListStore)
	tree.Connect("row-activated", func(_ *gtk.TreeView, path *gtk.TreePath, _ *gtk.TreeViewColumn) {
		iter, err := PoolsCartListStore.GetIter(path)
		if err != nil {
			rhdl.Printf("unable to get iter for path %v: %v", path, err)
		}
		val, err := PoolsCartListStore.GetValue(iter, 0)

		cart, err := val.GoValue()
		if err != nil {
			rhdl.Printf("unable to get go value for cart column: %v", err)
			return
		}

		cartStr, ok := cart.(string)
		if !ok {
			rhdl.Printf("list-store cart is not a string")
			return
		}

		cart64, err := strconv.ParseUint(cartStr, 10, 32)
		if err != nil {
			rhdl.Printf("invalid cart in tree view: %v", err)
			return
		}
		if err := p.Load(uint(cart64), 1); err != nil {
			rhdl.Println(err) // TODO: feedback at GUI?
			return
		}
		if err := p.Play(); err != nil {
			rhdl.Println(err) // TODO: feedback at GUI?
		}
	})

	var sw *gtk.ScrolledWindow
	if sw, err = gtk.ScrolledWindowNew(nil, nil); err != nil {
		return
	}
	sw.SetHExpand(true)
	sw.SetVExpand(true)
	sw.Add(tree)

	grid.Attach(sw, 1, 2, 1, 1)
	return
}

func getPoolsSelectorWidget(db *rddb.DB, p *player.Player) (gtk.IWidget, error) {
	grid, err := gtk.GridNew()
	if err != nil {
		return nil, err
	}
	grid.SetRowSpacing(25)
	grid.SetHExpand(true)
	grid.SetVExpand(true)

	if err = addPoolComboBox(grid, db, p); err != nil {
		return nil, err
	}
	if err = addPoolCartList(grid, db, p); err != nil {
		return nil, err
	}

	var frame *gtk.Frame
	if frame, err = gtk.FrameNew(""); err != nil {
		return nil, err
	}
	if err = setCssStyle(&frame.Widget, ".frame { border: 0; padding: 25px 42px; }"); err != nil {
		return nil, err
	}

	frame.Add(grid)

	return frame, nil
}
