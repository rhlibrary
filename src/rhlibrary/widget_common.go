//
//  rhlibrary
//
//  The Radio Helsinki Rivendell Library
//
//
//  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of rhlibrary.
//
//  rhlibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  rhlibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with rhlibrary. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"github.com/gotk3/gotk3/gtk"
)

func setCssStyle(widget *gtk.Widget, css string) (err error) {
	var cp *gtk.CssProvider
	if cp, err = gtk.CssProviderNew(); err != nil {
		return
	}
	if err = cp.LoadFromData(css); err != nil {
		return
	}
	var sc *gtk.StyleContext
	if sc, err = widget.GetStyleContext(); err != nil {
		return
	}
	sc.AddProvider(cp, gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
	return
}
