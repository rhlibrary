//
//  rhlibrary
//
//  The Radio Helsinki Rivendell Library
//
//
//  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of rhlibrary.
//
//  rhlibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  rhlibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with rhlibrary. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"fmt"
	"sort"
	"sync"
	"time"

	"code.helsinki.at/rhrd-go/player"
	"code.helsinki.at/rhrd-go/rddb"
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
)

var (
	jinglesMutex        sync.Mutex
	jingles             rddb.JingleList
	jinglesComboBox     *gtk.ComboBoxText
	jinglesCurrentGroup string
	jinglesCutListBin   *gtk.Bin
	jinglesCutList      *gtk.Grid
)

func fetchJingles(db *rddb.DB) (err error) {
	jinglesMutex.Lock()
	defer jinglesMutex.Unlock()

	if jingles, err = db.GetJingleList(rhuser.Username); err != nil {
		return err
	}
	sort.Sort(jingles)
	return
}

func updateJingles(db *rddb.DB) (err error) {
	if err = fetchJingles(db); err != nil {
		return
	}

	jinglesComboBox.RemoveAll()
	for _, jingle := range jingles {
		name := fmt.Sprintf("     %s", jingle.Title)
		jinglesComboBox.Append(jingle.Group, name)
	}

	if !jinglesComboBox.SetActiveID(jinglesCurrentGroup) {
		jinglesComboBox.SetActive(0)
	}
	return
}

func selectJingle() rddb.JingleListEntry {
	id := jinglesComboBox.GetActiveID()
	if id == "" {
		return rddb.JingleListEntry{}
	}
	jinglesCurrentGroup = id

	jinglesMutex.Lock()
	defer jinglesMutex.Unlock()

	for _, jingle := range jingles {
		if jingle.Group == jinglesCurrentGroup {
			return jingle
		}
	}
	return rddb.JingleListEntry{}
}

func addJingleComboBox(grid *gtk.Grid, db *rddb.DB, p *player.Player) (err error) {
	var box *gtk.Box
	if box, err = gtk.BoxNew(gtk.ORIENTATION_HORIZONTAL, 10); err != nil {
		return
	}

	var label *gtk.Label
	if label, err = gtk.LabelNew("Jingle Gruppe auswählen"); err != nil {
		return
	}
	box.PackStart(label, false, false, 0)

	if jinglesComboBox, err = gtk.ComboBoxTextNew(); err != nil {
		return
	}
	if err = updateJingles(db); err != nil {
		return err
	}
	jinglesComboBox.Connect("changed", func(_ *gtk.ComboBoxText) {
		jingle := selectJingle()
		if jingle.Group == "" {
			return
		}

		glib.IdleAdd(func() {
			if err = updateJingleCutListRows(jingle, db, p); err != nil {
				rhdl.Printf("error loading jingle cuts for %s: %v", jingle.Group, err)
				return
			}
		})
		rhdl.Printf("jingle group %s is now selected: %s", jingle.Group, jingle.Title)
	})

	box.PackStart(jinglesComboBox, true, true, 0)
	box.SetHExpand(true)

	grid.Attach(box, 1, 1, 1, 1)
	return
}

func getJingleCutListLabel(caption, css string) (label *gtk.Label, err error) {
	if label, err = gtk.LabelNew(caption); err != nil {
		return
	}
	if err = setCssStyle(&label.Widget, css); err != nil {
		return
	}

	return
}

func getJingleCutListEntryLabel(caption string, idx int) (label *gtk.Label, err error) {
	if (idx & 1) == 1 {
		return getJingleCutListLabel(caption, ".label { background-color: #D2D2D2; padding: 7px 15px; }")
	} else {
		return getJingleCutListLabel(caption, ".label { padding: 7px 15px; }")
	}
}

func getJingleCutListHeadLabel(caption string) (label *gtk.Label, err error) {
	return getJingleCutListLabel(caption, `
     .label {
        border-bottom: 2px solid;
        padding: 8px 15px;
        font-weight: bold;
     }`)
}

func addJingleCutListHead(idx int) (err error) {
	var lcut *gtk.Label
	if lcut, err = getJingleCutListHeadLabel("Cut #"); err != nil {
		return
	}
	jinglesCutList.Attach(lcut, 1, idx, 1, 1)

	var ltitle *gtk.Label
	if ltitle, err = getJingleCutListHeadLabel("Titel"); err != nil {
		return
	}
	ltitle.SetHExpand(true)
	jinglesCutList.Attach(ltitle, 2, idx, 1, 1)

	var lduration *gtk.Label
	if lduration, err = getJingleCutListHeadLabel("Länge"); err != nil {
		return
	}
	jinglesCutList.Attach(lduration, 3, idx, 1, 1)

	var limported *gtk.Label
	if limported, err = getJingleCutListHeadLabel("importiert"); err != nil {
		return
	}
	jinglesCutList.Attach(limported, 4, idx, 1, 1)

	var lplay *gtk.Label
	if lplay, err = getJingleCutListHeadLabel("Play"); err != nil {
		return
	}
	jinglesCutList.Attach(lplay, 5, idx, 1, 1)
	return
}

func getJingleCutListEntryPlayButton(cart uint, cut rddb.CutListEntry, p *player.Player, idx int) (gtk.IWidget, error) {
	btn, err := gtk.ButtonNewFromIconName("media-playback-start", gtk.ICON_SIZE_SMALL_TOOLBAR)
	if err != nil {
		return nil, err
	}
	btn.SetSizeRequest(35, 35)

	var frame *gtk.Frame
	if frame, err = gtk.FrameNew(""); err != nil {
		return nil, err
	}

	css := ".frame { border: 0; padding: 4px 15px; }"
	if (idx & 1) == 1 {
		css = ".frame { border: 0; background-color: #D2D2D2; padding: 4px 15px; }"
	}
	if err = setCssStyle(&frame.Widget, css); err != nil {
		return nil, err
	}
	frame.Add(btn)

	btn.Connect("clicked", func(_ *gtk.Button) {
		if err := p.Load(cart, cut.Number); err != nil {
			rhdl.Println(err) // TODO: feedback at GUI?
			return
		}
		if err := p.Play(); err != nil {
			rhdl.Println(err) // TODO: feedback at GUI?
		}
	})

	return frame, nil
}

func addJingleCutListEntry(idx int, cart uint, cut rddb.CutListEntry, p *player.Player) (err error) {
	var lcut *gtk.Label
	if lcut, err = getJingleCutListEntryLabel(fmt.Sprintf("%06d_%03d", cart, cut.Number), idx); err != nil {
		return
	}
	jinglesCutList.Attach(lcut, 1, idx, 1, 1)

	var ltitle *gtk.Label
	if ltitle, err = getJingleCutListEntryLabel(cut.Description, idx); err != nil {
		return
	}
	ltitle.SetHExpand(true)
	jinglesCutList.Attach(ltitle, 2, idx, 1, 1)

	duration := time.Unix(0, 0).UTC().Add(cut.Duration).Format("15:04:05.0")
	imported := "-"
	if cut.Imported.Valid {
		imported = cut.Imported.Time.Format("Mon 02.01.2006 15:04:05")
	}

	var lduration *gtk.Label
	if lduration, err = getJingleCutListEntryLabel(duration, idx); err != nil {
		return
	}
	jinglesCutList.Attach(lduration, 3, idx, 1, 1)

	var limported *gtk.Label
	if limported, err = getJingleCutListEntryLabel(imported, idx); err != nil {
		return
	}
	jinglesCutList.Attach(limported, 4, idx, 1, 1)

	var btn gtk.IWidget
	if btn, err = getJingleCutListEntryPlayButton(cart, cut, p, idx); err != nil {
		return
	}
	jinglesCutList.Attach(btn, 5, idx, 1, 1)
	return
}

func addJingleCutListSpacer(idx int) (err error) {
	var ltitle *gtk.Label
	if ltitle, err = getJingleCutListHeadLabel("deaktivirt"); err != nil {
		return
	}
	ltitle.SetHExpand(true)
	jinglesCutList.Attach(ltitle, 1, idx, 5, 1)

	return
}

func updateJingleCutListRows(jingle rddb.JingleListEntry, db *rddb.DB, p *player.Player) (err error) {
	if jinglesCutList != nil {
		jinglesCutListBin.Remove(jinglesCutList)
	}

	if jinglesCutList, err = gtk.GridNew(); err != nil {
		return
	}
	jinglesCutList.SetHExpand(true)
	jinglesCutList.SetVExpand(true)

	cart, err := db.GetJingleCutList(jingle)
	if err != nil {
		return err
	}

	idx := 1
	hasInactiveCuts := false
	addJingleCutListHead(idx)
	for _, cut := range cart.Cuts {
		if !cut.Evergreen {
			idx++
			addJingleCutListEntry(idx, cart.Number, cut, p)
		} else {
			hasInactiveCuts = true
		}
	}
	if hasInactiveCuts {
		idx++
		addJingleCutListSpacer(idx)
		for _, cut := range cart.Cuts {
			if cut.Evergreen {
				idx++
				addJingleCutListEntry(idx, cart.Number, cut, p)
			}
		}
	}

	jinglesCutListBin.Add(jinglesCutList)
	jinglesCutList.ShowAll()
	return
}

func addJingleCutList(grid *gtk.Grid, db *rddb.DB, p *player.Player) (err error) {
	var frame *gtk.Frame
	if frame, err = gtk.FrameNew(""); err != nil {
		return
	}
	jinglesCutListBin = &frame.Bin
	if err = setCssStyle(&frame.Widget, ".frame { border: 0; }"); err != nil {
		return
	}

	var sw *gtk.ScrolledWindow
	if sw, err = gtk.ScrolledWindowNew(nil, nil); err != nil {
		return
	}
	sw.SetHExpand(true)
	sw.SetVExpand(true)
	sw.Add(frame)

	grid.Attach(sw, 1, 2, 1, 1)
	return
}

func getJinglesSelectorWidget(db *rddb.DB, p *player.Player) (gtk.IWidget, error) {
	grid, err := gtk.GridNew()
	if err != nil {
		return nil, err
	}
	grid.SetRowSpacing(25)
	grid.SetHExpand(true)
	grid.SetVExpand(true)

	if err = addJingleComboBox(grid, db, p); err != nil {
		return nil, err
	}
	if err = addJingleCutList(grid, db, p); err != nil {
		return nil, err
	}

	var frame *gtk.Frame
	if frame, err = gtk.FrameNew(""); err != nil {
		return nil, err
	}
	if err = setCssStyle(&frame.Widget, ".frame { border: 0; padding: 25px 42px; }"); err != nil {
		return nil, err
	}

	frame.Add(grid)

	return frame, nil
}
