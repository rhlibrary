//
//  rhlibrary
//
//  The Radio Helsinki Rivendell Library
//
//
//  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of rhlibrary.
//
//  rhlibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  rhlibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with rhlibrary. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"fmt"

	"code.helsinki.at/rhrd-go/player"
	"code.helsinki.at/rhrd-go/rddb"
	"github.com/gotk3/gotk3/gtk"
)

type AppWindow struct {
	win    *gtk.Window
	db     *rddb.DB
	player *player.Player
}

func (aw *AppWindow) dispatchRequests() {
	for {
		select {}
	}
}

func (aw *AppWindow) drawSelector(grid *gtk.Grid) (err error) {
	widget, err := getSelectorWidget(aw.db, aw.player)
	if err == nil {
		grid.Add(widget)
	}
	return err
}

func (aw *AppWindow) drawPlayer(grid *gtk.Grid) error {
	widget, err := getPlayerWidget(aw.player)
	if err == nil {
		grid.Add(widget)
	}
	return err
}

// *********************************************************
// Public Interface

func (aw *AppWindow) ShowAndRun() {
	go aw.dispatchRequests()
	aw.win.ShowAll()
}

func NewAppWindow(db *rddb.DB, player *player.Player, width, height int) (aw *AppWindow, err error) {
	aw = &AppWindow{}
	aw.db = db
	aw.player = player
	if aw.win, err = gtk.WindowNew(gtk.WINDOW_TOPLEVEL); err != nil {
		return
	}
	aw.win.SetTitle(fmt.Sprintf("rhlibrary: logged in as %s", rhuser.Username))
	aw.win.Connect("destroy", func() {
		gtk.MainQuit()
	})

	var grid *gtk.Grid
	if grid, err = gtk.GridNew(); err != nil {
		return
	}
	grid.SetOrientation(gtk.ORIENTATION_VERTICAL)
	grid.SetRowSpacing(10)

	if err = aw.drawPlayer(grid); err != nil {
		return
	}
	if err = aw.drawSelector(grid); err != nil {
		return
	}

	var frame *gtk.Frame
	if frame, err = gtk.FrameNew(""); err != nil {
		return nil, err
	}
	if err = setCssStyle(&frame.Widget, ".frame { border: 0; padding: 15px;  }"); err != nil {
		return nil, err
	}

	frame.Add(grid)
	aw.win.Add(frame)

	aw.win.SetPosition(gtk.WIN_POS_CENTER)
	aw.win.SetDefaultSize(width, height)
	return
}
