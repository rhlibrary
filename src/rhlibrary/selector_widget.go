//
//  rhlibrary
//
//  The Radio Helsinki Rivendell Library
//
//
//  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of rhlibrary.
//
//  rhlibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  rhlibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with rhlibrary. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"code.helsinki.at/rhrd-go/player"
	"code.helsinki.at/rhrd-go/rddb"
	"github.com/gotk3/gotk3/gtk"
)

type getSelectorFunc func(*rddb.DB, *player.Player) (gtk.IWidget, error)

func addSelectorPage(nb *gtk.Notebook, title string, getWidget getSelectorFunc, db *rddb.DB, p *player.Player) (err error) {
	var page gtk.IWidget
	if page, err = getWidget(db, p); err != nil {
		return
	}

	var label *gtk.Label
	if label, err = gtk.LabelNew(title); err != nil {
		return
	}
	label.SetSizeRequest(250, 0) // workaround until expanding tabs works...

	if err = setCssStyle(&label.Widget, ".label { font-weight: bold; font-size: 1.1em; padding: 3px; }"); err != nil {
		return
	}

	nb.AppendPage(page, label)
	return
}

func getSelectorWidget(db *rddb.DB, p *player.Player) (gtk.IWidget, error) {
	nb, err := gtk.NotebookNew()
	if err != nil {
		return nil, err
	}

	if err = addSelectorPage(nb, "Sendungen", getShowsSelectorWidget, db, p); err != nil {
		return nil, err
	}

	if err = addSelectorPage(nb, "Jingles", getJinglesSelectorWidget, db, p); err != nil {
		return nil, err
	}

	if err = addSelectorPage(nb, "Musikpools", getPoolsSelectorWidget, db, p); err != nil {
		return nil, err
	}

	nb.Connect("switch-page", func(_ *gtk.Notebook, _ gtk.IWidget, pagenum uint) {
		switch pagenum {
		case 0:
			updateShows(db)
		case 1:
			updateJingles(db)
		case 2:
			updatePools(db)
		default:
			rhl.Printf("selector error invalid page: %d", pagenum)
		}
	})

	if err = setCssStyle(&nb.Widget, ".notebook.header { background-color: unset; }"); err != nil {
		return nil, err
	}

	nb.SetProperty("show-border", false)
	nb.SetHExpand(true)
	nb.SetVExpand(true)

	return nb, nil
}
