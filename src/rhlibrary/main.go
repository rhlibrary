//
//  rhlibrary
//
//  The Radio Helsinki Rivendell Library
//
//
//  Copyright (C) 2016 Christian Pointner <equinox@helsinki.at>
//
//  This file is part of rhlibrary.
//
//  rhlibrary is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  any later version.
//
//  rhlibrary is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with rhlibrary. If not, see <http://www.gnu.org/licenses/>.
//

package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/user"

	"code.helsinki.at/rhrd-go/player"
	"code.helsinki.at/rhrd-go/rddb"
	"github.com/gotk3/gotk3/gtk"
)

const (
	volumeFactor = 2.82 // +9db
)

var (
	rhl    = log.New(os.Stderr, "[rhlibrary]\t", log.LstdFlags)
	rhdl   = log.New(ioutil.Discard, "[rhlibrary-dbg]\t", log.LstdFlags)
	rhuser *user.User
)

func init() {
	if _, exists := os.LookupEnv("RHLIBRARY_DEBUG"); exists {
		rhdl.SetOutput(os.Stderr)
	}

	var err error
	if rhuser, err = user.Current(); err != nil {
		panic(fmt.Sprintf("can't fetch current user: %s", err.Error()))
	}
}

type envStringValue string

func newEnvStringValue(key, dflt string) *envStringValue {
	if envval, exists := os.LookupEnv(key); exists {
		return (*envStringValue)(&envval)
	} else {
		return (*envStringValue)(&dflt)
	}
}

func (s *envStringValue) Set(val string) error {
	*s = envStringValue(val)
	return nil
}

func (s *envStringValue) Get() interface{} { return string(*s) }

func (s *envStringValue) String() string { return fmt.Sprintf("%s", *s) }

func main() {
	rdconf := newEnvStringValue("RHLIBRARY_RD_CONF", "/etc/rd.conf")
	flag.Var(rdconf, "rdconf", "path to the Rivendell config file (environment: RHLIBRARY_RD_CONF)")
	sndDir := newEnvStringValue("RHLIBRARY_SND_DIR", "/var/snd")
	flag.Var(sndDir, "snddir", "path to the Rivendell snd directory (environment: RHLIBRARY_SND_DIR)")
	help := flag.Bool("help", false, "show usage")

	flag.Parse()
	if *help {
		flag.Usage()
		return
	}

	gtk.Init(nil)

	db, err := rddb.NewDBReadOnly(rdconf.Get().(string))
	if err != nil {
		rhl.Println("Error initializing Rivdenll DB:", err)
		return
	}
	defer db.Cleanup()

	p, err := player.NewPlayer(sndDir.Get().(string), volumeFactor, rhl, rhdl)
	if err != nil {
		rhl.Println("Error initializing Player:", err)
		return
	}

	mw, err := NewAppWindow(db.GetInterface(), p.GetInterface(), 1200, 800)
	if err != nil {
		rhl.Println("Error initializing Main Window:", err)
		return
	}

	rhl.Printf("rhlibrary started as user: %s", rhuser.Username)
	mw.ShowAndRun()

	gtk.Main()
}
